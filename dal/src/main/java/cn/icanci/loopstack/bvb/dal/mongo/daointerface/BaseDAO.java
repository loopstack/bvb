package cn.icanci.loopstack.bvb.dal.mongo.daointerface;

import java.util.List;

import cn.icanci.loopstack.bvb.dal.mongo.common.PageList;
import cn.icanci.loopstack.bvb.dal.mongo.dateobject.BaseDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 17:53
 */
public interface BaseDAO<T extends BaseDO> {

    /** 文档对应的名字  */
    String BASE_COLLECTION_NAME = "bvb-";

    /**
     * 插入文档一条记录
     *
     * @param t t
     */
    void insert(T t);

    /**
     * 更新文档一条记录
     *
     * @param t t
     */
    void update(T t);

    /**
     * 查询文档所有记录
     *
     * @return 返回查询的结果
     */
    List<T> queryAll();

    /**
     * 查询文档所有记录
     *
     * @param t 请求参数
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return 返回查询的结果
     */
    PageList<T> pageQuery(T t, int pageNum, int pageSize);

    /**
     * 根据 _id 查询一条信息
     *
     * @param _id _id
     * @return 返回查询的结果
     */
    T queryOneById(String _id);

    /** 基本表 */
    interface BaseColumn {
        /** 文档id  */
        String _ID         = "_id";
        /** uuid  */
        String UUID        = "uuid";
        /** createTime  */
        String CREATE_TIME = "createTime";
        /** updateTime  */
        String UPDATE_TIME = "updateTime";
        /** 状态，是否删除 */
        String DELETED     = "deleted";
        /** 操作环境  */
        String ENV         = "env";
    }
}
