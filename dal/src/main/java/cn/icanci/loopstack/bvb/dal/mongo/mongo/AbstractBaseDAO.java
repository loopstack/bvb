package cn.icanci.loopstack.bvb.dal.mongo.mongo;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;

import cn.icanci.loopstack.bvb.dal.mongo.daointerface.BaseDAO;
import cn.icanci.loopstack.bvb.dal.mongo.dateobject.BaseDO;
import cn.icanci.loopstack.bvb.dal.mongo.utils.EnvUtils;
import cn.icanci.loopstack.bvb.dal.mongo.utils.IDHolder;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 18:00
 */
public abstract class AbstractBaseDAO<T extends BaseDO> extends MongoPageHelper implements BaseDAO<T>, InitializingBean {

    protected String DEFAULT_ENV;

    @Override
    public void insert(T t) {
        // 处理插入数据
        doInsert(t);
    }

    @Override
    public void update(T t) {
        t.setUpdateTime(new Date());
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        DEFAULT_ENV = EnvUtils.getEnv();
    }

    public void batchInsert(List<T> list) {
        // 处理插入数据
        list.forEach(this::doInsert);
    }

    private void doInsert(T t) {
        t.setId(null);
        t.setDeleted(false);
        t.setCreateTime(new Date());
        t.setUpdateTime(new Date());
        t.setEnv(DEFAULT_ENV);
        t.setUuid(IDHolder.generateNoBySnowFlakeDefaultPrefix());
    }
}
