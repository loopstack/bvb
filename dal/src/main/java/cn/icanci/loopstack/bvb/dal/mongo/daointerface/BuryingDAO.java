package cn.icanci.loopstack.bvb.dal.mongo.daointerface;

import java.util.List;

import cn.icanci.loopstack.bvb.dal.mongo.daointerface.BaseDAO;
import cn.icanci.loopstack.bvb.dal.mongo.dateobject.BuryingDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 17:55
 */
public interface BuryingDAO extends BaseDAO<BuryingDO> {
    /** 文档对应的名字  */
    String           COLLECTION_NAME  = BASE_COLLECTION_NAME + "burying";
    /** 文档对应的Class */
    Class<BuryingDO> COLLECTION_CLASS = BuryingDO.class;

    /**
     * 批量插入
     * 
     * @param buryingList buryingList
     */
    void batchInsert(List<BuryingDO> buryingList);

    /** 列 */
    interface BuryingColumn extends BaseColumn {
        /** TraceId */
        String TRACE_ID    = "traceId";
        /** 业务标识 */
        String BUSINESS_NO = "businessNo";
        /** 系统应用唯一标识 */
        String SYSTEM_KEY  = "systemKey";
        /** 模块 */
        String MODULE      = "module";
        /** 时间 */
        String TIME        = "time";
        /** 执行数据 */
        String MESSAGE     = "message";
    }

}
