package cn.icanci.loopstack.bvb.dal.mongo.utils.service.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Service;

import cn.icanci.loopstack.bvb.dal.mongo.utils.EnvUtils;
import cn.icanci.loopstack.bvb.dal.mongo.utils.service.EnvService;


/**
 * @author icanci
 * @since 1.0 Created in 2022/11/12 08:29
 */
@Service("envService")
public class EnvServiceImpl implements EnvService, BeanPostProcessor {
    @Value("${bvb.env}")
    private String env;

    @Override
    public String getEnv() {
        return env;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        EnvUtils.setEnvService(this);
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
