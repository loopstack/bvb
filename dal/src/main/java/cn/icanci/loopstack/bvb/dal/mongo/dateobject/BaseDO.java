package cn.icanci.loopstack.bvb.dal.mongo.dateobject;

import java.util.Date;
import java.util.StringJoiner;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 17:52
 */
public class BaseDO {
    /** mongodb id */
    @Id
    private String  id;
    /** 雪花算法随机UUID  */
    private String  uuid;
    /** 创建时间 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date    createTime;
    /** 更新时间  */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date    updateTime;
    /** 状态 false 有效，true 无效 */
    private boolean deleted;
    /** 环境 */
    private String  env;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("id=" + id).add("uuid=" + uuid).add("createTime=" + createTime).add("updateTime=" + updateTime).add("deleted=" + deleted).add("env=" + env)
            .toString();
    }
}
