package cn.icanci.loopstack.bvb.biz.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

import cn.icanci.loopstack.bvb.common.model.BuryingVO;
import cn.icanci.loopstack.bvb.dal.mongo.dateobject.BuryingDO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 18:15
 */
@Mapper(componentModel = "spring", uses = {}, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface BuryingMapper extends BaseMapper<BuryingDO, BuryingVO> {
}
