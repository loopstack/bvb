package cn.icanci.loopstack.bvb.biz.service;

import java.util.List;

import cn.icanci.loopstack.bvb.common.model.BuryingVO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 15:40
 */
public interface BuryingService {
    /**
     * 批量插入
     * 
     * @param buryingList buryingList
     */
    void batchInsert(List<BuryingVO> buryingList);

    /**
     * 查询所有数据
     * 
     * @return 返回所有数据
     */
    List<BuryingVO> queryAll();
}
