package cn.icanci.loopstack.bvb.biz.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import cn.icanci.loopstack.bvb.biz.service.BuryingService;
import cn.icanci.loopstack.bvb.biz.mapper.BuryingMapper;
import cn.icanci.loopstack.bvb.common.model.BuryingVO;
import cn.icanci.loopstack.bvb.dal.mongo.daointerface.BuryingDAO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 15:40
 */
@Service
public class BuryingServiceImpl implements BuryingService {
    @Resource
    private BuryingDAO    buryingDAO;
    @Resource
    private BuryingMapper buryingMapper;

    @Override
    public void batchInsert(List<BuryingVO> buryingList) {
        if (CollectionUtils.isEmpty(buryingList)) {
            return;
        }
        buryingDAO.batchInsert(buryingMapper.vos2dos(buryingList));
    }

    @Override
    public List<BuryingVO> queryAll() {
        return buryingMapper.dos2vos(buryingDAO.queryAll());
    }
}
