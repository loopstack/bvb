package cn.icanci.loopstack.bvb.biz.mapper;

import java.util.Collection;
import java.util.List;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 18:15
 */
public interface BaseMapper<T, R> {

    R do2vo(T t);

    List<R> dos2vos(Collection<T> ts);

    T vo2do(R r);

    List<T> vos2dos(List<R> rs);
}
