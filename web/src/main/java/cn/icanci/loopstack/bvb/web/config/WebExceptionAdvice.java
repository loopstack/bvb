package cn.icanci.loopstack.bvb.web.config;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.icanci.loopstack.bvb.common.result.R;

/**
 * 异常处理器
 *
 * @author icanci
 * @since 1.0 Created in 2022/11/12 11:19
 */
@ControllerAdvice
public class WebExceptionAdvice {

    /**
     * 非跳转页面无权限异常
     *
     * @param e 异常
     * @return 返回异常信息
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public R jsonHandlerException(Exception e) {
        return R.builderFail().message(e.getMessage()).build();
    }

}
