package cn.icanci.loopstack.bvb.web.api;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.icanci.loopstack.bvb.biz.service.BuryingService;
import cn.icanci.loopstack.bvb.common.result.R;
import cn.icanci.loopstack.bvb.web.model.BuryingModelVO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 18:18
 */
@RestController
@RequestMapping("/bicApi/burying")
public class BuryingApi {
    @Resource
    private BuryingService buryingService;

    @PostMapping("/batchSubmit")
    public R batchSubmit(@RequestBody BuryingModelVO buryingModel) {
        buryingService.batchInsert(buryingModel.getBuryingList());
        return R.builderOk().build();
    }
}
