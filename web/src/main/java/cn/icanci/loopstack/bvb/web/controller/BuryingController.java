package cn.icanci.loopstack.bvb.web.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.icanci.loopstack.bvb.biz.service.BuryingService;
import cn.icanci.loopstack.bvb.common.result.R;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 18:34
 */
@RestController
@RequestMapping("/bicAdmin/burying")
public class BuryingController {
    @Resource
    private BuryingService buryingService;

    @GetMapping("queryAll")
    public R queryAll() {
        return R.builderOk().data("list", buryingService.queryAll()).build();
    }
}
