package cn.icanci.loopstack.bvb.web.model;

import java.io.Serializable;
import java.util.List;
import java.util.StringJoiner;

import cn.icanci.loopstack.bvb.common.model.BuryingVO;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 18:20
 */
public class BuryingModelVO implements Serializable {

    private List<BuryingVO> buryingList;

    public List<BuryingVO> getBuryingList() {
        return buryingList;
    }

    public void setBuryingList(List<BuryingVO> buryingList) {
        this.buryingList = buryingList;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("buryingList=" + buryingList).toString();
    }
}
