package cn.icanci.loopstack.bvb.views;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 20:47
 */
@ComponentScan(basePackages = { "cn.icanci.loopstack.bvb" })
@SpringBootApplication
public class BvbApplication {
    public static void main(String[] args) {
        SpringApplication.run(BvbApplication.class, args);
    }
}
