import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/bvbAdmin/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/bvbAdmin/user/info/' + token,
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/bvbAdmin/user/logout',
    method: 'post'
  })
}
