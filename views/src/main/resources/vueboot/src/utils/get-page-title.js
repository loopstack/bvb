import defaultSettings from '@/settings'

const title = defaultSettings.title || 'DDCA-集群计算'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
