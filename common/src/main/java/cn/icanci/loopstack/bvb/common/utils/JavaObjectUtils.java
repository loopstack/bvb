package cn.icanci.loopstack.bvb.common.utils;

import org.apache.lucene.util.RamUsageEstimator;

/**
 * @author icanci
 * @since 1.0 Created in 2023/08/19 16:00
 */
public class JavaObjectUtils {

    private static final int SIZE_OF_KB = 1024;

    private JavaObjectUtils() {
    }

    public static long sizeOfByte(Object obj) {
        if (obj == null) {
            return 0;
        }
        return RamUsageEstimator.sizeOf(obj);
    }

    public static long sizeOfKb(Object obj) {
        long byteSize = sizeOfByte(obj);
        return byteSize / SIZE_OF_KB;
    }

    public static long sizeOfMb(Object obj) {
        long kbSize = sizeOfKb(obj);
        return kbSize / SIZE_OF_KB;
    }
}
