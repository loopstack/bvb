package cn.icanci.loopstack.bvb.common.model;

import java.util.StringJoiner;

/**
 * 埋点模型
 * 
 * @author icanci
 * @since 1.0 Created in 2023/08/19 14:43
 */
public class BuryingVO extends BaseVO {
    /** TraceId  */
    private String traceId;
    /** 业务标识 */
    private String businessNo;
    /** 系统应用唯一标识 */
    private String systemKey;
    /** 模块 */
    private String module;
    /** 时间 */
    private long   time;
    /** 执行数据  */
    private String message;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getBusinessNo() {
        return businessNo;
    }

    public void setBusinessNo(String businessNo) {
        this.businessNo = businessNo;
    }

    public String getSystemKey() {
        return systemKey;
    }

    public void setSystemKey(String systemKey) {
        this.systemKey = systemKey;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return new StringJoiner(",").add("traceId=" + traceId).add("businessNo=" + businessNo).add("systemKey=" + systemKey).add("module=" + module).add("time=" + time)
            .add("message=" + message).toString();
    }
}
